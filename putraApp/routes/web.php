<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\bookController;
use App\Http\Controllers\pinjamController;
use App\Http\Controllers\memberController;
use App\Http\Controllers\pengembalianController;
use App\Http\Controllers\PostController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/dashboard', function () {
    return view('index');
});


Route::resource('books', bookController::class);

Route::resource('members', memberController::class);

Route::resource('pinjams',pinjamController::class);

Route::resource('pengembalians', pengembalianController::class);

Route::resource('posts', PostController::class);