@include('layout/header', ['title' => 'Data buku'])   
<div class="container pt-4 bg-white">
    
        <h2>Data Buku Perpustakaan</h2>
        <a href = "{{route('books.create')}}" class="btn btn-info">Create</a>

        @include('flash-message')
                
        <table class="table table-hover">
            <tr>
                <th>Kode Buku</th>
                <th>Judul</th>
                <th>Penerbit</th>
                <th>Pengarang</th>
                <th>Jumlah</th>
                <th>Action</th>
            </tr>
            @foreach ($books as $book )   
                <tr>
                    <td>{{ $book->kode_buku }}</td>
                    <td>{{ $book->judul_buku }}</td>
                    <td>{{ $book->penerbit_buku }}</td>
                    <td>{{ $book->pengarang_buku }}</td>
                    <td>{{ $book->jumlah_buku }}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                        <form action="{{ route('books.destroy', [$book]) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="{{ route('books.edit', [$book]) }}" class="btn btn-primary">Edit</a>
                            <button class="btn btn-primary">Delete</button>
                        </form>
                        </div>
                    </td>
                </tr>
            @endforeach
        </table>
</div>
@include('layout/footer')