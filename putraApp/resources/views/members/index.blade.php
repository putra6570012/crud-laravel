@include('layout/header', ['title' => 'Data Anggota'])   
<div class="container pt-4 bg-white">
        <h2>Data Anggota Perpustakaan</h2>
        <a href = "{{route('members.create')}}" class="btn btn-info">Create</a>
        @include('flash-message')
        <table class="table table-hover">
            <tr>
                <th>Kode Anggota</th>
                <th>Nama Anggota</th>
                <th>KTP</th>
                <th>Email</th>
                <th>No Telepon</th>
                <th>Tanggal Daftar</th>
                <th>Action</th>
            </tr>

            @foreach ($members as $member )   
                <tr>
                    <td>{{ $member->kode_anggota }}</td>
                    <td>{{ $member->nama_anggota }}</td>
                    <td>{{ $member->KTP }}</td>
                    <td>{{ $member->Email }}</td>
                    <td>{{ $member->nomor_telepon }}</td>
                    <td>{{ $member->tanggal_daftar }}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                        <form action="{{ route('members.destroy', [$member]) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="{{ route('members.edit', [$member]) }}" class="btn btn-primary">Edit</a>
                            <button class="btn btn-primary">Delete</button>
                        </form>
                        </div>
                    </td>
                </tr>
                @endforeach
        </table>
</div>
@include('layout/footer')