@include('layout/header', ['title' => 'Data Pengembalian'])  
<div class="container pt-4 bg-white">
        <h2>Data Pengembalian Buku</h2>
        <table class="table table-hover">
        <a href = "{{route('pengembalians.create')}}" class="btn btn-info">Create</a>
        @include('flash-message')
            <tr>
                <th>Kode Pengembalian</th>
                <th>Tanggal Pinjam</th>
                <th>Tanggal Kembali</th>
                <th>Keterlambatan</th>
                <th>Denda</th>
                <th>Action</th>
            </tr>
            @foreach ($pengembalians as $pengembalian)
            <tr>
                <td>{{ $pengembalian->kode_pengembalian }}</td>
                <td>{{ $pengembalian->tanggal_pinjam }}</td>
                <td>{{ $pengembalian->tanggal_kembali }}</td>
                <td>{{ $pengembalian->keterlambatan }}</td>
                <td>{{ $pengembalian->denda }}</td>
                <td>
                    <div class="btn-group" role="group" aria-label="Basic example">
                    <form action="{{ route('pengembalians.destroy', [$pengembalian]) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="{{ route('pengembalians.edit', [$pengembalian]) }}" class="btn btn-primary">Edit</a>
                        <button class="btn btn-primary">Delete</button>
                    </form>
                    </div>
                </td>
            </tr>
        @endforeach
    </table>
</div>
    @include('layout/footer')