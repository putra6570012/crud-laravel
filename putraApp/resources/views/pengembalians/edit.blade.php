@include('layout/header', ['title' => 'Data Edit Pengembalian'])  
    <div class="container pt-4 bg-white">
    <h2>Form Pengembalian Buku</h2>
    @if ($errors->any()) 
            <div class="alert alert-danger"> 
                <ul> 
                    @foreach ($errors->all() as $error) 
                        <li>{{ $error }}</li> 
                        @endforeach 
                </ul> 
            </div> 
            @endif 
            <form action="{{ route('pengembalians.update', $pengembalian->id) }}" method="post"> 
                @csrf
                @method('PUT')
        <div class="mb-3">
            <label class="form-label">Kode_Pengembalian</label>
            <input type="text" class="form-control" name="kode_pengembalian" value="{{ $pengembalian->kode_pengembalian }}" readonly>
        </div>
        <div class="mb-3">
            <label class="form-label">Tanggal Pinjam</label>
            <input type="date" class="form-control" name="tanggal_pinjam">
        </div>
        <div class="mb-3">
            <label class="form-label">Tanggal Kembali</label>
            <input type="date" class="form-control" name="tanggal_kembali">
        </div>
        <div class="mb-3">
            <label class="form-label">Periode</label>
            <select name="keterlambatan" class="form-control">
              <option>Keterlambatan</option>
              <option value="Terlambat">Terlambat</option>
              <option value="Tepat Waktu">Tepat Waktu</option>
            </select>
        </div>
        <div class="mb-3">
            <label class="form-label">Denda</label>
            <input type="text" class="form-control" name="denda">
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div>
    @include('layout/footer')