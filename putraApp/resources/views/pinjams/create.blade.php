@include('layout/header', ['title' => 'Form Data Peminjaman']) 
    <div class="container pt-4 bg-white">
    <h2>Form Peminjaman Buku</h2>
    @if ($errors->any()) 
            <div class="alert alert-danger"> 
                <ul> 
                    @foreach ($errors->all() as $error) 
                        <li>{{ $error }}</li> 
                        @endforeach 
                </ul> 
            </div> 
            @endif 
            <form action="{{ route('pinjams.store') }}" method="post"> 
                @csrf
        <div class="mb-3">
            <label class="form-label">Kode Peminjaman</label>
            <input type="text" class="form-control" name="kode_pinjam">
        </div>
        <div class="mb-3">
            <label class="form-label">Nama Anggota</label>
            <input type="text" class="form-control" name="nama_anggota">
        </div>
        <div class="mb-3">
            <label class="form-label">Tanggal Pinjam</label>
            <input type="date" class="form-control" name="tanggal_pinjam">
        </div>
        <div class="mb-3">
            <label class="form-label">Tanggal Kembali</label>
            <input type="date" class="form-control" name="tanggal_kembali">
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div>
    @include('layout/footer')