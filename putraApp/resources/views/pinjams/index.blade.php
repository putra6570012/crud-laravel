@include('layout/header', ['title' => 'Data Peminjaman'])   
<div class="container pt-4 bg-white">

        <h2>Data Peminjaman Buku</h2>
        <a href = "{{route('pinjams.create')}}" class="btn btn-info">Create</a>

        @include('flash-message')

        <table class="table table-hover">
            <tr>
                <th>Kode Peminjaman</th>
                <th>Nama Anggota</th>
                <th>Tanggal Pinjam</th>
                <th>Tanggal Kembali</th>
                <th>Action</th>
            </tr>
            @foreach ($pinjams as $pinjam)
            <tr>
                <td>{{ $pinjam->kode_pinjam }}</td>
                <td>{{ $pinjam->nama_anggota }}</td>
                <td>{{ $pinjam->tanggal_pinjam }}</td>
                <td>{{ $pinjam->tanggal_kembali }}</td>
                <td>
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <form action="{{ route('pinjams.destroy', [$pinjam]) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="{{ route('pinjams.edit', [$pinjam]) }}" class="btn btn-primary">Edit</a>
                            <button class="btn btn-primary">Delete</button>
                        </form>
                        </div>
                    </td>
                </tr>
            @endforeach
        </table>
</div>
@include('layout/footer')