<footer class="bg-dark py-3 text-white mt-auto">
    <div class="container-fluid text-center">
        Sistem Informasi Perpustakaan | Copyright {{date("Y")}} Furuta
    </div>
</footer>
</body>
</html>