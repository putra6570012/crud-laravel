<?php

namespace App\Http\Controllers;

use App\Models\pinjam;
use Illuminate\Http\Request;

class pinjamController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pinjams = pinjam::all();
        return view('pinjams.index', ['pinjams' => $pinjams]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pinjams.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'kode_pinjam' => 'required',
            'nama_anggota' => 'required',
            'tanggal_pinjam' => 'required',
            'tanggal_kembali' => 'required',
        ]);

        //simpan ke database
        $pinjam = pinjam::create([
            'kode_pinjam' => $request->input ('kode_pinjam'),
            'nama_anggota' => $request->input ('nama_anggota'),
            'tanggal_pinjam' => $request->input ('tanggal_pinjam'),
            'tanggal_kembali' => $request->input ('tanggal_kembali'),
        ]);

        //flash message
        if($pinjam){
            return redirect()->route('pinjams.index')->with('success','Data Berhasil Disimpan');
        }
        else{
            return redirect()->back()->with('error','Data Gagal Disimpan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(pinjam $pinjam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $pinjam = pinjam::find($id);
        return view('pinjams.edit', ['pinjam' => $pinjam]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, pinjam $pinjam)
    {
        $validated = $request->validate([
            'kode_pinjam' => 'required',
            'nama_anggota' => 'required',
            'tanggal_pinjam' => 'required',
            'tanggal_kembali' => 'required',
        ]);

        //simpan ke database
        $pinjam->update([
            'kode_pinjam' => $request->input ('kode_pinjam'),
            'nama_anggota' => $request->input ('nama_anggota'),
            'tanggal_pinjam' => $request->input ('tanggal_pinjam'),
            'tanggal_kembali' => $request->input ('tanggal_kembali'),
        ]);

        //flash message
        if($pinjam){
            return redirect()->route('pinjams.index')->with('success','Data Berhasil Diperbaruhi');
        }
        else{
            return redirect()->back()->with('error','Data Gagal Diperbaruhi');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $pinjams = pinjam::destroy($id);
        $pinjams = pinjam::all();
        return view('pinjams.index', ['pinjams' => $pinjams]);
    }
}
