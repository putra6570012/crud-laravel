<?php

namespace App\Http\Controllers;

use App\Models\pengembalian;
use Illuminate\Http\Request;

class pengembalianController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pengembalians = pengembalian::all();
        return view('pengembalians.index', ['pengembalians' => $pengembalians]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view ('pengembalians.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'kode_pengembalian' => 'required',
            'tanggal_kembali' => 'required',
            'tanggal_pinjam' => 'required',
            'keterlambatan' => 'required',
            'denda' => 'required',
        ]);

        //simpan ke database
        $pengembalian = Pengembalian::create([
            'kode_pengembalian' => $request->input ('kode_pengembalian'),
            'tanggal_kembali' => $request->input ('tanggal_kembali'),
            'tanggal_pinjam' => $request->input ('tanggal_pinjam'),
            'keterlambatan' => $request->input ('keterlambatan'),
            'denda' => $request->input ('denda'),
        ]);

        //flash message
        if($pengembalian){
            return redirect()->route('pengembalians.index')->with('success','Data Berhasil Disimpan');
        }
        else{
            return redirect()->back()->with('error','Data Gagal Disimpan');
        }
    }


    /**
     * Display the specified resource.
     */
    public function show(pengembalian $pengembalian)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $pengembalian = pengembalian::find($id);
        return view('pengembalians.edit', ['pengembalian' => $pengembalian]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, pengembalian $pengembalian)
    {
        $validated = $request->validate([
            'kode_pengembalian' => 'required',
            'tanggal_kembali' => 'required',
            'tanggal_pinjam' => 'required',
            'keterlambatan' => 'required',
            'denda' => 'required',
        ]);

        //simpan ke database
        $pengembalian->update([
            'kode_pengembalian' => $request->input ('kode_pengembalian'),
            'tanggal_kembali' => $request->input ('tanggal_kembali'),
            'tanggal_pinjam' => $request->input ('tanggal_pinjam'),
            'keterlambatan' => $request->input ('keterlambatan'),
            'denda' => $request->input ('denda'),
        ]);

        //flash message
        if($pengembalian){
            return redirect()->route('pengembalians.index')->with('success','Data Berhasil Diperbarui');
        }
        else{
            return redirect()->back()->with('error','Data Gagal Diperbarui');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
    
        $pengembalians = pengembalian::destroy($id);
        $pengembalians = pengembalian::all();   
        return view('pengembalians.index', ['pengembalians' => $pengembalians]);
    }
}
