<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pengembalian extends Model
{
    use HasFactory;
    protected $fillable = ['kode_pengembalian','tanggal_pinjam','tanggal_kembali','keterlambatan','denda'];
}
