<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pinjam extends Model
{
    use HasFactory;
    protected $fillable = ['kode_pinjam','nama_anggota','tanggal_pinjam','tanggal_kembali'];
}
